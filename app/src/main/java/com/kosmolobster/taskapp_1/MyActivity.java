package com.kosmolobster.taskapp_1;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;


public class MyActivity extends Activity {
    private static int MATRIX_DIMENSION = 20;
    private static int SPIRAL_MATRIX_DIMENSION = 1001;
    private static int MULTIPLICATION_COUNT = 3; // for zero-based calculations

    List<List<Integer>> matrix;
    int[][] spiralMatrix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        InputStream inputStream = getApplicationContext().getResources().openRawResource(R.raw.matrix);
        Scanner scanFile = new Scanner(new DataInputStream(inputStream));
        List<Integer> numbers = new ArrayList<Integer>();

        while (scanFile.hasNext()){
            String number = scanFile.next();
            numbers.add(Integer.parseInt(number));
        }

        matrix = new LinkedList<List<Integer>>();
        for (int i = 0; i < numbers.size(); i += MATRIX_DIMENSION) {
            matrix.add(numbers.subList(i,
                    i + Math.min(MATRIX_DIMENSION, numbers.size() - i)));
        }

        int maximumNumber = getMax();
        ArrayList<int[]> dupl = getDuplicates(maximumNumber);
        if (!dupl.equals(null)) {
            int MAX = getMaxMultiplication(dupl);
            TextView textView = (TextView) findViewById(R.id.summResult);
            textView.setText(String.valueOf(MAX));
            Log.d("taskapp_1", "MAX multiplication of 4 numbers: " + String.valueOf(MAX));
        }

        fillSpiralMatrix();
        int diag = getMainDiagonalsSumm(spiralMatrix);
        TextView textView = (TextView) findViewById(R.id.multiResult);
        textView.setText(String.valueOf(diag));
        Log.d("taskapp_1", "Spiral matrix diag summ: " + String.valueOf(diag));
    }

    public int getMax() {
        int max = 0;

        for(int row = 0; row < matrix.size(); ++row){
            int current_max = Collections.max(matrix.get(row));
            if (current_max >= max) {
                max = current_max;
            }
        }

        return max;
    }

    public <T> List<T> slice(List<T> list, int index, int count) {
        List<T> result = new ArrayList<T>();
        if (index >= 0 && index < list.size()) {
            int end = index + count < list.size() ? index + count : list.size();
            for (int i = index; i < end; i++) {
                result.add(list.get(i));
            }
        }
        return result;
    }

    public ArrayList<int[]> getDuplicates(int number) {
        ArrayList<int[]> duplicatesArray = new ArrayList<int[]>();

        for(int row = 0; row < matrix.size(); ++row){
            for (int column = 0; column < matrix.get(row).size(); ++column) {
                if (matrix.get(row).get(column).equals(number)) {
                    int[] duplicate = new int[3];
                    duplicate[0] = number;
                    duplicate[1] = row;
                    duplicate[2] = column;

                    duplicatesArray.add(duplicate);
                }
            }
        }

        return duplicatesArray;
    }

    public int getMaxMultiplication(ArrayList<int[]> duplicates) {
        ArrayList<Integer> allBiggers = new ArrayList<Integer>();

        for(int i = 0; i < duplicates.size(); ++i){
            ArrayList<Integer> biggers = new ArrayList<Integer>();

            int[] item = duplicates.get(i);

            biggers.add(getMaxRowMultiplication(item));
            biggers.add(getMaxColumnMultiplication(item));
            biggers.add(getLeftDiagMultiplication(item));
            biggers.add(getRightDiagMultiplication(item));

            allBiggers.add(Collections.max(biggers));
        }

        return Collections.max(allBiggers);
    }

    public int getMaxRowMultiplication(int[] item) {
        List<Integer> results = new ArrayList<Integer>();
        List<Integer> row = matrix.get(item[1]);
        int column = item[2];

        for (int i = 0; i < MULTIPLICATION_COUNT + 1; ++i) {
            if (column - MULTIPLICATION_COUNT + i >= 0 ) {
                List<Integer> slicced = slice(row, column - MULTIPLICATION_COUNT + i, column + MULTIPLICATION_COUNT + 1);

                int multi = 1;
                for( int j = 0; j < MULTIPLICATION_COUNT + i; ++j ) {
                    multi *= slicced.get(j);
                }
                results.add(multi);
            }
        }

        return Collections.max(results);
    }

    public int getMaxColumnMultiplication(int[] item) {
        List<Integer> results = new ArrayList<Integer>();
        List<Integer> column = new ArrayList<Integer>();
        int row = item[1];

        for (int i = 0; i < MULTIPLICATION_COUNT + 1; ++i) {
            if (row - MULTIPLICATION_COUNT + 1 + i >= 0) {
                column.clear();
                for( int j = row - MULTIPLICATION_COUNT + i; j < row + MULTIPLICATION_COUNT + 1; ++j ) {
                    if ( j < MATRIX_DIMENSION && j > 0) {
                        Log.d("taskapp_1", "row: " + row);
                        Log.d("taskapp_1", "j: " + j);
                        Log.d("taskapp_1", "row + MULTIPLICATION_COUNT + 1: " + (row + MULTIPLICATION_COUNT + 1));
                        column.add(matrix.get(j).get(item[2]));
                    }
                }

                int multi = 1;
                for( int k = 0; k < MULTIPLICATION_COUNT + 1; ++k ) {
                    multi *= column.get(k);
                }
                results.add(multi);
            }
        }

        return Collections.max(results);
    }

    public int getLeftDiagMultiplication(int[] item) {
        List<Integer> results = new ArrayList<Integer>();
        //List<Integer> diagonal = new ArrayList<Integer>();
        int multi = 1;

        for (int i = 0; i < MULTIPLICATION_COUNT + 1; ++i) {
            int curr_row = item[1] - MULTIPLICATION_COUNT + i;
            int curr_col = item[2] - MULTIPLICATION_COUNT + i;
            if (curr_row >= 0 && curr_col >= 0 ){

                for( int j = 0; j < MULTIPLICATION_COUNT + 1 ; ++j ) {
                    if (curr_col + j < MATRIX_DIMENSION && curr_row + j < MATRIX_DIMENSION) {
                        //diagonal.add(matrix.get(curr_row + j).get(curr_col + j));
                        multi *= matrix.get(curr_row + j).get(curr_col + j);
                    }
                }

                results.add(multi);
            }
        }

        return Collections.max(results);
    }

    public int getRightDiagMultiplication(int[] item) {
        List<Integer> results = new ArrayList<Integer>();
        List<Integer> diagonal = new ArrayList<Integer>();
        int multi = 1;

        for (int i = 0; i < MULTIPLICATION_COUNT + 1; ++i) {
            int curr_row = item[1] + MULTIPLICATION_COUNT + i;
            int curr_col = item[2] + MULTIPLICATION_COUNT + i;
            if (curr_row >= 0 && curr_col >= 0 ){

                for( int j = 0; j < MULTIPLICATION_COUNT + 1 ; ++j ) {
                    if (curr_col - j < MATRIX_DIMENSION && curr_row - j < MATRIX_DIMENSION) {
                        diagonal.add(matrix.get(curr_row - j).get(curr_col - j));
                        multi *= matrix.get(curr_row - j).get(curr_col - j);
                    }
                }

                results.add(multi);
            }
        }

        return Collections.max(results);
    }



    public void fillSpiralMatrix() {
        //SPIRAL_MATRIX_DIMENSION = 5;
        spiralMatrix = new int[SPIRAL_MATRIX_DIMENSION][SPIRAL_MATRIX_DIMENSION];
        int positionX = SPIRAL_MATRIX_DIMENSION/2;
        int positionY = SPIRAL_MATRIX_DIMENSION % 2 == 0 ? (SPIRAL_MATRIX_DIMENSION/2 - 1) : (SPIRAL_MATRIX_DIMENSION/2);
        int direction = 0;
        int stepsCount = 1;
        int stepPosition = 0;
        int stepChange = 0;

        for (int i = 0; i < Math.sqrt(SPIRAL_MATRIX_DIMENSION); ++i) {
            spiralMatrix[positionX][positionY] = i;
            if (stepPosition < stepsCount) {
                ++stepPosition;
            } else {
                stepPosition = 1;
                if (stepChange == 1) {
                    ++stepsCount;
                }

                stepChange = (stepChange + 1) % 2;
                direction = (direction + 1) % 4;
            }

            switch (direction){
                case 0:
                    ++positionX;
                    break;
                case 1:
                    --positionY;
                    break;
                case 2:
                   --positionX;
                    break;
                case 3:
                    ++positionY;
                    break;
            }
        }
    }

    public int getMainDiagonalsSumm(int[][] matrix) {
        int summ = 0;

        for (int row = 0; row < matrix.length - 1; row++)
        {
            for (int col = 0; col < matrix.length - 1; col++)
                summ += matrix[row][col];
        }

        return summ;
    }
}
